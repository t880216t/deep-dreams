+++
template = "page.html"
title = "About Deep Dreams"
+++

# About

Deep Dreams is an AI generated podcast with nonsensical stories to help you fall asleep
to a soothing voice. Let your robotic overlords whisper comforting sweet nothings
straight into your subconscious. What could go wrong?

If you're looking for the code, you've come to the right place:

[https://gitlab.com/stavros/deep-dreams](https://gitlab.com/stavros/deep-dreams)
