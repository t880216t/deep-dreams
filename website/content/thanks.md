+++
template = "page.html"
title = "Deep Thanks"
+++

# Deep Thanks

Lots of people helped make Deep Dreams.
Here, I would like to thank some whose contributions stood out:

* Many thanks to my friend [Theodore](https://sirodoht.com/) for setting up this website
  and for generally being fantastic.
* Many thanks are also due to [Emmanuel](https://news.ycombinator.com/user?id=bambax)
  for generating the background music that's in every episode.
