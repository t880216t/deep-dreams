Deep Dreams
===========

This is the code that generates [Deep Dreams podcast
episodes](https://deepdreams.stavros.io) from a script.

Since this is all very ad-hoc and bound to change at any time, there isn't much
documentation, but generally:

* Run `./gen_script.py <some prompt>` to generate a script.
* Run `./gen_episode.py 3` to generate episode 3 from the script in the `episodes/` directory into the `audio/` directory.
* The site is somehow updated from the podcast feed. Somehow.
