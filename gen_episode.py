#!/usr/bin/env python3
import argparse
import json
import os
import random
import re
import subprocess
import tempfile
from typing import List

import pydub
from azure.cognitiveservices.speech import AudioDataStream
from azure.cognitiveservices.speech import SpeechConfig
from azure.cognitiveservices.speech import SpeechSynthesisOutputFormat
from azure.cognitiveservices.speech import SpeechSynthesizer

# Generate the voice here so it doesn't change mid-file.
VOICE = random.choice(["en-US-ChristopherNeural", "en-GB-SoniaNeural"])


def generate_audio(text, outfile):
    ssml_text = f"""
<speak version="1.0" xmlns="https://www.w3.org/2001/10/synthesis" xml:lang="en-GB">
  <voice name="{VOICE}">
  <prosody rate="0.85">

{text}

  </prosody>
  </voice>
</speak>
"""
    speech_config = SpeechConfig(
        subscription=json.load(open("keys.json"))["azure"],
        region="westeurope",
    )

    speech_config.set_speech_synthesis_output_format(
        SpeechSynthesisOutputFormat["Audio48Khz192KBitRateMonoMp3"]
    )
    synthesizer = SpeechSynthesizer(speech_config=speech_config, audio_config=None)

    result = synthesizer.speak_ssml_async(ssml_text).get()
    if not result._audio_data:
        raise ValueError("Audio came back empty.")
    stream = AudioDataStream(result)
    stream.save_to_wav_file(outfile)


def split_into_chunks(text: str, max_length) -> List[str]:
    """Split a chunk of text into chunks of max_length and return a list of them."""
    sentences = re.split(r"(?<=\.)\s+(?=[A-Z])", text.replace("\n", " "))
    chunks = []
    current_chunk: List[str] = []
    chunk_length = 0
    for sentence in sentences:
        sentence_length = len(sentence)
        if chunk_length + sentence_length + 1 > max_length:
            # This chunk would overflow, make a new chunk.
            chunks.append(" ".join(current_chunk))
            current_chunk = []
            chunk_length = 0

        current_chunk.append(sentence)
        chunk_length += sentence_length + 1
    chunks.append(" ".join(current_chunk))
    return chunks


def add_background_track(episode: str, background: str, output: str) -> None:
    tempbg = tempfile.mkstemp()[1]
    tempepisode = tempfile.mkstemp()[1]

    aepisode = pydub.AudioSegment.from_mp3(episode)
    print(f"Episode duration is {int(aepisode.duration_seconds / 60)} minutes.")
    abackground = pydub.AudioSegment.from_mp3(background)

    apadded_episode = (
        pydub.AudioSegment.silent(duration=7000)
        + aepisode
        + pydub.AudioSegment.silent(duration=8000)
    )
    apadded_episode.export(tempepisode, format="mp3")

    acut_bg = (
        abackground[: apadded_episode.duration_seconds * 1000]
        .fade_in(3000)
        .fade_out(5000)
    )

    # Lower the background track volume.
    alower_volume_cut_bg = acut_bg - 10

    alower_volume_cut_bg.export(tempbg, format="mp3")

    subprocess.run(
        [
            "/usr/bin/env",
            "ffmpeg",
            "-y",
            "-i",
            tempbg,
            "-i",
            tempepisode,
            "-filter_complex",
            "amerge,acompressor=threshold=-21dB:ratio=12:attack=100:release=500",
            "-ac",
            "2",
            "-c:a",
            "libmp3lame",
            "-q:a",
            "4",
            output,
        ]
    )
    os.unlink(tempbg)
    os.unlink(tempepisode)


def concatenate_tracks(inputs: List[str], output) -> None:
    """Concatenate mutliple audio tracks into one."""
    outfile = pydub.AudioSegment.from_mp3(inputs[0])
    for infile in inputs[1:]:
        outfile = outfile + pydub.AudioSegment.from_mp3(infile)
    outfile.export(output, format="mp3")

    for filename in inputs:
        print(f"Deleting {filename}...")
        os.unlink(filename)


def main(episode_number: int):
    with open(f"episodes/episode{episode_number}.txt") as infile:
        text = f"""
Welcome to Episode {episode_number} of the Deep Dreams podcast, the
AI generated podcast with nonsensical stories
to help you sleep. I am nobody, and I will be narrating your story tonight.

<break time="3s" />

{infile.read()}

<break time="3s" />

I hope you have enjoyed the Deep Dreams podcast and have not actually heard this part
because you are asleep.
""".strip()

    episode_filename = f"audio/episode-{episode_number}"
    filenames = []
    for counter, chunk in enumerate(split_into_chunks(text, 4500)):
        filename = f"{episode_filename}_{counter}.mp3"
        print(f"Generating {filename}...")
        generate_audio(chunk, filename)
        filenames.append(filename)

    print("Concatenating parts...")
    concatenate_tracks(filenames, f"{episode_filename}_bare.mp3")

    print("Adding background track...")
    add_background_track(
        episode=f"{episode_filename}_bare.mp3",
        background="audio/background.mp3",
        output=f"{episode_filename}.mp3",
    )
    os.unlink(f"{episode_filename}_bare.mp3")
    print("Done.")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Generate a Deep Dreams episode.")
    parser.add_argument(
        "episode_number", type=int, help="The number of the episode to generate."
    )

    args = parser.parse_args()
    main(args.episode_number)
