#!/usr/bin/env python3
"""
Get GPT-3 to generate some text.

Due to the way that GPT-3 charges for text (ie by considering the prompt in the cost),
this script tries to maximize the amount of text generated per prompt. It does this by
getting the last few tokens in the existing script, using those as the prompt and
continuing.
"""
import argparse
import json
import re
import unicodedata
from pathlib import Path
from typing import List

import openai
from colorama import Fore
from colorama import Style

openai.api_key = json.load(open("keys.json"))["openai"]


def count_tokens(text: str) -> int:
    """Approximate the number of tokens in `text`."""
    return int(len(re.findall(r"""(\w+|[\.';\!\?:,\"]+)""", text)) * 1.1)


def normalize(text: str) -> str:
    return unicodedata.normalize("NFKD", text.strip())


def main(filename: Path, topic: str = "", engine: str = "davinci") -> None:
    filename.touch()
    with filename.open("r") as script:
        text = script.read().strip()

    sentences = re.split(r"""(?<=[\.\!\"\?])(\s+)(?=[A-Z])""", text)
    prompt_sentences: List[str] = []
    prompt = (
        "This is a very long, pleasant and calming story for helping children fall asleep. It is very detailed and long. "
        + topic
        + " It goes like this: "
    )
    tokens = count_tokens(prompt)
    for sentence in reversed(sentences):
        prompt_sentences.insert(0, sentence)
        tokens += count_tokens(sentence)
        if tokens > 480:
            break
    prompt += "".join(prompt_sentences)

    print(Fore.GREEN + "Prompt:" + Style.RESET_ALL)
    print(prompt)

    response = openai.Completion.create(
        engine=engine,
        prompt=prompt.strip(),
        temperature=0.9,
        max_tokens=1450,
        top_p=1,
        frequency_penalty=0.2,
        presence_penalty=0.1,
    )

    generated = response["choices"][0]["text"]
    print(Fore.GREEN + "\nResponse: " + Style.RESET_ALL)
    print(generated)
    with filename.open("w") as script:
        script.write(normalize(text + generated))
    token_count = count_tokens(text + " " + generated)
    print(f"Tokens: {Fore.GREEN}{token_count}{Style.RESET_ALL}")
    print(f"Estimated time: {Fore.GREEN}{int(token_count / 190)} min{Style.RESET_ALL}")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Generate a Deep Dreams script.")
    parser.add_argument("topic", help="The topic of the story.")
    parser.add_argument(
        "-e", "--engine", help="The GPT-3 engine to use.", default="davinci"
    )

    args = parser.parse_args()

    file = Path("script.txt")
    main(file, topic=args.topic.strip(), engine=args.engine)
